package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.sql.mapper.DefaultRowMapper;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PurchaseOrderDao;
import com.yunfeisoft.business.model.PurchaseOrder;
import com.yunfeisoft.business.model.Supplier;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseOrderDaoImpl
 * Description: 采购单信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class PurchaseOrderDaoImpl extends ServiceDaoImpl<PurchaseOrder, String> implements PurchaseOrderDao {

    @Override
    public Page<PurchaseOrder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("po.createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("po.orgId", params.get("orgId"));
            wb.andEquals("po.payStatus", params.get("payStatus"));
            wb.andFullLike("s.name", params.get("supplierName"));
        }

        SelectBuilder builder = getSelectBuilder("po");
        builder.column("s.name as supplierName")
                .join(Supplier.class).alias("s").on("po.supplierId = s.id");

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public PurchaseOrder queryTotalAmount(String orgId, int status, int payStatus) {
        String sql = "SELECT SUM(TOTAL_AMOUNT_) AS TOTAL_AMOUNT_, SUM(PAY_AMOUNT_) AS PAY_AMOUNT_ FROM TT_PURCHASE_ORDER WHERE ORG_ID_ = ? AND STATUS_ = ? AND PAY_STATUS_ = ? AND IS_DEL_ = 2";
        List<PurchaseOrder> list = jdbcTemplate.query(sql, new Object[]{orgId, status, payStatus}, new DefaultRowMapper<PurchaseOrder>(PurchaseOrder.class));
        return CollectionUtils.isEmpty(list) ? new PurchaseOrder() : list.get(0);
    }
}