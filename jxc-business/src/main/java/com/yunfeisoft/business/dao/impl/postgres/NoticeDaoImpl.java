package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.NoticeDao;
import com.yunfeisoft.business.model.Notice;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ClassName: NoticeDaoImpl
 * Description: 公告信息Dao实现
 * Author: Jackie liu
 * Date: 2020-08-23
 */
@Repository
public class NoticeDaoImpl extends ServiceDaoImpl<Notice, String> implements NoticeDao {

    @Override
    public Page<Notice> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithAsc("num");
        wb.addOrderByWithDesc("createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("type", params.get("type"));
            wb.andFullLike("title", params.get("title"));
        }
        return queryPage(wb);
    }
}