package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.sql.mapper.DefaultRowMapper;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.SaleOrderDao;
import com.yunfeisoft.business.model.Customer;
import com.yunfeisoft.business.model.SaleOrder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: SaleOrderDaoImpl
 * Description: 销售单信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class SaleOrderDaoImpl extends ServiceDaoImpl<SaleOrder, String> implements SaleOrderDao {

    @Override
    public Page<SaleOrder> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("so.createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("so.orgId", params.get("orgId"));
            wb.andEquals("so.payStatus", params.get("payStatus"));
            wb.andFullLike("c.name", params.get("customerName"));
        }

        SelectBuilder builder = getSelectBuilder("so");
        builder.column("c.name as customerName")
                .join(Customer.class).alias("c").on("so.customerId = c.id");

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<SaleOrder> queryByIds(String[] ids) {
        if (ArrayUtils.isEmpty(ids)) {
            return new ArrayList<>();
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andIn("id", ids);
        return query(wb);
    }

    @Override
    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus) {
        String sql = "SELECT SUM(TOTAL_AMOUNT_) AS TOTAL_AMOUNT_, SUM(PAY_AMOUNT_) AS PAY_AMOUNT_ FROM TT_SALE_ORDER WHERE ORG_ID_ = ? AND STATUS_ = ? AND PAY_STATUS_ = ? AND IS_DEL_ = 2";
        List<SaleOrder> list = jdbcTemplate.query(sql, new Object[]{orgId, status, payStatus}, new DefaultRowMapper<SaleOrder>(SaleOrder.class));
        return CollectionUtils.isEmpty(list) ? new SaleOrder() : list.get(0);
    }
}