package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.Notice;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: NoticeService
 * Description: 公告信息service接口
 * Author: Jackie liu
 * Date: 2020-08-23
 */
public interface NoticeService extends BaseService<Notice, String> {

    public Page<Notice> queryPage(Map<String, Object> params);
}