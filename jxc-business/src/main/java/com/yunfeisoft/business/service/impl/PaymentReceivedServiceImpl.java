package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PaymentReceivedDao;
import com.yunfeisoft.business.model.PaymentReceived;
import com.yunfeisoft.business.service.inter.PaymentReceivedService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * ClassName: PaymentReceivedServiceImpl
 * Description: 收付款信息service实现
 * Author: Jackie liu
 * Date: 2020-08-11
 */
@Service("paymentReceivedService")
public class PaymentReceivedServiceImpl extends BaseServiceImpl<PaymentReceived, String, PaymentReceivedDao> implements PaymentReceivedService {

    @Override
    @DataSourceChange(slave = true)
    public Page<PaymentReceived> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }
}