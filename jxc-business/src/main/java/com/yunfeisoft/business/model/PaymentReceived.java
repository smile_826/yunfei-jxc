package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ClassName: PaymentReceived
 * Description: 收付款信息
 *
 * @Author: Jackie liu
 * Date: 2020-08-11
 */
@Entity
@Table(name = "TT_PAYMENT_RECEIVED")
public class PaymentReceived extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 类型(1收款，2付款)
     */
    @Column
    private Integer type;

    /**
     * 类目
     */
    @Column
    private String name;

    /**
     * 金额
     */
    @Column
    private BigDecimal amount;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date occurDate;

    public String getTypeStr() {
        if (type == null) {
            return null;
        } else if (type == 1) {
            return "收款";
        } else if (type == 2) {
            return "付款";
        }
        return null;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getOccurDate() {
        return occurDate;
    }

    public void setOccurDate(Date occurDate) {
        this.occurDate = occurDate;
    }
}