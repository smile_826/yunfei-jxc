<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑公告信息</title>
	<#include "/common/vue_resource.ftl">
	<#include "/common/upload.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">标题<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.title" placeholder="请输入标题" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">所属栏目<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<select v-model="record.type" class="layui-input">
						<option value="">请选择</option>
						<option value="index">首页</option>
						<option value="sale_order_add">销售开单</option>
						<option value="sale_order_list">销售单管理</option>
						<option value="purchase_order_add">进货开单</option>
						<option value="purchase_order_list">进货单管理</option>
						<option value="indec_order_add">损益开单</option>
						<option value="indec_order_list">损益单管理</option>
						<option value="allot_order_add">调拨开单</option>
						<option value="allot_order_list">调拨单管理</option>
						<option value="sale_order_chart">销售汇总</option>
						<option value="purchase_order_chart">采购汇总</option>
						<option value="date_chart">日期汇总</option>
						<option value="product_stock_alarm">库存明细</option>
						<option value="product_list">商品管理</option>
						<option value="supplier_list">供应商管理</option>
						<option value="customer_list">客户管理</option>
						<option value="company_info">公司信息</option>
						<option value="product_trace">商品追查</option>
						<option value="user_list">账号管理</option>
						<option value="role_list">权限管理</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">内容<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<textarea v-model="record.content" placeholder="请输入内容" class="layui-textarea"></textarea>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">视频<#--<span class="ui-request">*</span>--></label>
				<div class="layui-input-block">
					<div style="display1:inline-block;vertical-align1:top;" v-if="record.videoUrl">
						已上传视频文件
					</div>
					<div style="display:inline-block;vertical-align:top;">
						<a id="select-video-button">选择文件</a>
						<div class="layui-form-mid layui-word-aux">只能上传mp4文件，且不超过50MB</div>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">排序号</label>
				<div class="layui-input-block">
					<input v-model="record.num" class="layui-input"/>
					<div class="layui-form-mid layui-word-aux">数字越小越靠上</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				title: '',
				type: '',
				content: '',
				videoUrl: '',
				num: ''
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/
				$.upload({
					renderId:"#select-video-button",
					accept:{title:'Images',extensions:'mp4',mimeTypes:'video/mp4'},
					fileSingleSizeLimit: 50 * 1024 * 1024,//10M
					uploadSuccess:function (file, data) {
						if (!data.success) {
							$.message(data.message);
							return;
						}
						var result = data.data[0];
						that.record.videoUrl = result.path;
					}
				});
			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/notice/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/notice/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
