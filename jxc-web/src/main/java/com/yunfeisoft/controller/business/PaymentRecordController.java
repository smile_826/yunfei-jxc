package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.PaymentRecord;
import com.yunfeisoft.business.service.inter.PaymentRecordService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: PaymentRecordController
 * Description: 付款信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class PaymentRecordController extends BaseController {

    @Autowired
    private PaymentRecordService paymentRecordService;

    /**
     * 添加付款信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentRecord/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(PaymentRecord record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "purchaseOrderId", "订单号为空");
        //validator.required(request, "payAmount", "实付金额为空");
        validator.number(request, "payAmount", "实付金额不合法");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        if (record.getPayAmount() == null || record.getPayAmount().compareTo(BigDecimal.ZERO) == 0) {
            return ResponseUtils.success("保存成功");
            //return ResponseUtils.warn("实付金额不能小于0");
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        paymentRecordService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 批量付款信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentRecord/batchSave", method = RequestMethod.POST)
    @ResponseBody
    public Response batchSave(HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "purchaseOrderIds", "订单号为空");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        /*if (record.getPayAmount().compareTo(BigDecimal.ZERO) < 0) {
            return ResponseUtils.warn("实付金额不能小于0");
        }*/

        User user = ApiUtils.getLoginUser();
        String purchaseOrderIds = ServletRequestUtils.getStringParameter(request, "purchaseOrderIds", null);
        paymentRecordService.batchSave2(user.getOrgId(), purchaseOrderIds.split(","));
        return ResponseUtils.success("保存成功");
    }

    /**
     * 分页查询付款信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentRecord/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String supplierName = ServletRequestUtils.getStringParameter(request, "supplierName", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("supplierName", supplierName);

        Page<PaymentRecord> page = paymentRecordService.queryPage(params);
        return ResponseUtils.success(page);
    }

}
